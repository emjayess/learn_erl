-module(useless).
-export([add/2, greet/0, greet_and_add_two/1]).

add(A,B) ->
    A + B.

%% Shows greetings
%% io:format/1 is the standard function used to output text
greet() ->
    io:format("ohai erl!~n").

%% Show greetings and add 2 to X
greet_and_add_two(X) ->
    greet(),
    add(X,2).